
facts
  xpositive(string)
  xnegative(string)

predicates
   nondeterm animal(string) 
   nondeterm it(string)
   nondeterm ask(string,string)
   nondeterm remember(string,string)
   nondeterm positive(string)
   nondeterm negative(string)
   clear_facts 
   run 

clauses
 
/* ��������� ������������ */
 
  positive(X):- xpositive(X),!. 
  positive(X):- not(xnegative(X)), ask(X,yes),!.

  negative(X):- xnegative(X),!.
  negative(X):- not(xpositive(X)), ask(X,no),!.

  ask(X,yes):-
	write("�������� ", X, "?  "),
	readln(S),
	remember(X,S),
  	S="yes".

  ask(X,no):-
	write("�������� ", X, "?  "),
	readln(S),
        remember(X,S),
  	S="no".
  	
  remember(X,yes):-asserta(xpositive(X)).
  remember(X,no):-asserta(xnegative(X)).
  
  clear_facts:-
	retractall(xpositive(_)),
        retractall(xnegative(_)).

  run:-
        nl,write("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"),
        nl,write("*                          ���������� �������                            *"),
  	nl,write("*                          ���������� ��������                                 *"),
   	nl,write("*----------------------------------------------------------------------------------------------*"),
   	nl,write("*              ����������, �������� �� �����������               *"),
   	nl,write("*                             � ����� 'yes' ���  'no'                          *"),
   	nl,write("* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *"),
   	nl,nl,  
	animal(X),!,
	write("��������, ���� �������� - ",X),
	nl,
	clear_facts.
	
  run:- write("����������, ���������� ���� ��������"), nl, clear_facts.
 
/* ������� ���� ������ */
 
  animal("������"):-			
	it("�������������"),	
	it("������"),
	positive("����� ��������-���������� ����"),
	positive("����� ������ �����").

  animal("����"):-
	it("�������������"),
	it("������"),
	positive("����� ��������-���������� ����"),
	positive("����� ������ ������").

  animal("�����"):-
        it("�������������"),
	it("��������"),
	positive("����� ������� ���"),
	positive("����� ������� ����"),
	positive("����� ������ �����").

  animal("�����"):-
        it("�������������"),
	it("��������"),
	positive("����� ������ ������").

  animal("������"):-
	it("�����"),
	negative("����� ������"),
	positive("����� ������� ���"),
	positive("����� ������� ����").

  animal("�������"):-
	it("�����"),
	negative("����� ������"),
	positive("����� �������"),
	positive("����� �����-����� �����").

  animal("���������"):-
	it("�����"),	
	positive("����� ������"),
	positive("����� ������� ����").

  it("�������������"):-
	positive("����� ������"),
	positive("����� ������ ������").

  it("�����"):-
	positive("����� �����"),
	positive("����� ����������� ����").

  it("������"):-
	positive("����� ���� ����"),
	positive("����� �����"),
	positive("����� �����"),	
        positive("����� ������������ ������ ����� ").

  it("��������"):-	
	positive("����� ������"),
	positive("�������� �������").

goal
  run.